import numpy as np
from os import path
# kolar=np.load('./kolar.npy',allow_pickle=True)
path_to_dat = path.abspath(path.join(path.dirname(__file__), 'kolar.npy'))
kolar=np.load(path_to_dat,allow_pickle=True)

######################################
### FUNCTION ON KOLAR (np.ndarray) ###
######################################
def ensure_int(fn):
    def wrapper(sample_number):
        return fn(int(sample_number))
    return wrapper

# @ensure_int #compatible with neither multiple arguments nor strings
def fetch(sample_number,numbering_direction='column'):
    # fonction pour récupérer la position dans les plaques 1 et 2 (c-a-d 5) 
    # pour les 192 premiers samples de la premiere batch
    try:
        sample_number=int(sample_number)
        # print('the arg is a num')
        name=_num2name(sample_number)
    except:
        name=sample_number
        sample_number=int(_name2num(sample_number))
        # print('the arg was a name and is now a num ',sample_number)
    sample_number-=1
    row_name=list('ABCDEFGH')
    row,col,plate=0,0,1
    if numbering_direction=='column':
        for n in range(sample_number):
            if row%7==0 and bool(row):
                row=0
                if col%11==0 and bool(col):
                    col=0
                    plate+=4
                else:
                    col+=1
            else:
                row+=1
        return f'Sample {sample_number+1}; Gene {name}; Plate {plate}: {row_name[row]}{col+1}'

@ensure_int #not compatible with multiple arguments
def get_plate_position_list(sample_number,numbering_direction='column'):
    sample_number-=1
    row,col,plate=0,0,1
    if numbering_direction=='column':
        for n in range(sample_number):
            if row%7==0 and bool(row):
                row=0
                if col%11==0 and bool(col):
                    col=0
                    plate+=4
                else:
                    col+=1
            else:
                row+=1
        return plate,row+1,col+1

def _name2num(gene):
    # Trouver le # a partir du nom 
    # hardcodé pour les 2 premières plaques
    pos=np.where(kolar==gene)
    npos=((int(pos[2])-1)*8)+int(pos[1])
    if int(pos[0]) == 5:
        npos+=96
    if gene != kolar[get_plate_position_list(npos)]:
        return 'Not in plate 1 or 5'
    else:
        return f'{npos}'
    
def _num2name(gene):
    return kolar[get_plate_position_list(gene)]