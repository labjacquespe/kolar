#interface Keio cOLlection ARray (KOLAR) v1.0

###compiled with:
# pyinstaller kolar.py --onefile --clean -y --add-data="named_dict.pickle:." --add-binary="kolar.npy:."
from AnnotationInterface import *
from KeioTemplate import *
import sys, time

def help():
    print("""
#####################################
### Keio cOLlection ARray (KOLAR) ###
#####################################

(1) is_gene <str> 
        ask DB if this is a valid gene name or bnumber (output: True/False)
(2) get_bnum <str>
        return the up-to-date bnumber of this gene name (output: bnumber)
(3) get_name <str>
        return the up-to-date gene name of this bnumber (output: gene name)
(4) get_aliases <str>
        return the list of names for this gene name or bnumber (output: gene name list)
(5) fetch <int/str>
        return sample number, gene name and plate position of the provided gene or the sample number
        !WARNING only supporting plates 1 and 5!
(6) help
        display this manual
(7) quit
        exit this program

USAGE : call the function by its name or number, followed by the proper <argument>:
    $ is_gene thrL
    >>>    True
    $ where_is 100
    >>>    plate 2: D1

""")

def initialize():
    print(r"""
    _ __ ___  _    ___  ___ 
    | / /| . || |  | . || . \
    |  \ | | || |_ |   ||   /
    |_\_\`___'|___||_|_||_\_\
                                                        
        """)

def main():
    initialize()
    help()
    # if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    #     print('running in a PyInstaller bundle')
    # else:
    #     print('running in a normal Python process')
    query=True
    func_dict={'1':'is_gene',
        '2':'get_bnum',
        '3':'get_name',
        '4':'get_aliases',
        '5':'fetch',
        '6':'help',
        '7':'quit'}
    print("What do you want? (function <argument>)")
    while query:
        query=input("$ ").split(' ')
        if query[0].isnumeric():
            query[0] = func_dict[query[0]]
        if "help" in query:
            globals()[query[0]]()
        elif "quit" in query or "exit" in query:
            print('Thank you come again')
            time.sleep(1.5)
            sys.exit(0)
        elif len(query) == 2:
            try:
                print('>>>',globals()[query[0]](query[1]))
            except:
                print('Invalid function/query')
                pass
        else:
            print('Invalid function/query')
        # if not query[0]:
        #     break

if __name__ == "__main__":
    main()