import pickle
from os import path
##################################################
### FUNCTION ON ANNOTATION DICTIONARY (pyDICT) ###
##################################################

path_to_dat = path.abspath(path.join(path.dirname(__file__), 'named_dict.pickle'))
with open(path_to_dat, 'rb') as handle:
    named = pickle.load(handle)
# with open('./named_dict.pickle', 'rb') as handle:
#     named = pickle.load(handle)

def is_gene(bnum, data=named):
    if list(data.keys()).count(bnum) >=1:
        return True
    else:
        return _is_gene_nm(bnum)

def _is_gene_nm(name,data=named):
    for k,v in data.items():
        if name in v:
            return True
    return False

def get_bnum(name,data=named):
    _bnumlist=[]
    for k,v in data.items():
        if name in v or name in k:
            _bnumlist.append(k)
    if len(_bnumlist)>1:
        return name+'_'+'_'.join(_bnumlist)
    elif len(_bnumlist)<1:
        return False
    else:
        return ''.join(_bnumlist)

def get_aliases(query, data=named):
    if len(query) != 5 or (query[0] != 'b' and query[0] != 'c'):
        nquery=get_bnum(query)
        if nquery:
            query=nquery
    if is_gene(query):
        return data[get_bnum(query)]
    else:
        return [query]

def get_name(query, data=named):
    return get_aliases(query)[0]