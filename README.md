#####################################

     _ __ ___  _    ___  ___ 
    | / /| . || |  | . || . \
    |  \ | | || |_ |   ||   /
    |_\_\`___'|___||_|_||_\_\
                                                     
#####################################

## Description ##
KOLAR (Keio cOLlection ARray) is a portable and efficient Python tool designed to ease the surveying of the Keio collection and E. coli gene name synonyms based on the Refseq 2022 annotation. It eliminates the need for manual spreadsheet consultation, saving you valuable time and effort.

More specifically, KOLAR is an easy-to-use commandline wrapper of a tridimentional numpy array depicting the Rodrigue's lab Keio collection as its physical arrangement at present time (05/2023). The executables were compiled using PyInstaller on three operating system (Ubuntu 20, Windows 10 and MacOS Montery), hence KOLAR is compatible with those. Currently, the plates that are filled with monogenic mutants are numbered with odd numbers and sorted by descending fitness, leaving the even numbered plates empty.

With its user-friendly command-line interface, KOLAR lay on a three-dimensional numpy array representing the physical arrangement of the Rodrigue's lab Keio collection as of May 2023. In this array, filled plates containing monogenic mutants are numbered with odd numbers and sorted by descending fitness levels, leaving the even numbered plates empty.

The array and the functions described below are compiled with PyInstaller into executables compatible with Ubuntu 20, Windows 10 and MacOS Monterey.

## Functions ##
```
(1) is_gene <str> 
        ask DB if this is a valid gene name or bnumber (output: True/False)
(2) get_bnum <str>
        return the up-to-date bnumber of this gene name (output: bnumber)
(3) get_name <str>
        return the up-to-date gene name of this bnumber (output: gene name)
(4) get_aliases <str>
        return the list of names for this gene name or bnumber (output: gene name list)
(5) fetch <int/str>
        return sample number, gene name and plate position of the provided gene or the sample number
        !WARNING only supporting plates 1 and 5 (i.e. sample 1 to 192)!
(6) help
        display this manual
(7) quit
        exit this program
```

## Installation
Download the appropriate executable for you operating system and run it.

If you wish to compile it for another operating system, you will need the bin folder. Follow the README in there, it is very simple.

## Usage ##
Call the function by its name or number, followed by the proper argument (case sensitive):
```
    $ is_gene thrL
    >>>    True
    $ fetch 100
    >>>    plate 2: D1
```
## Roadmap ##
There is standardization work going on, where the plates numbering and sample naming with drastically change, and the remaining plates will be integrated in a block.
Also, there is a possibility that a simple GUI is deployed at some point.
