1) To compile you need Python >=3.8 (https://www.python.org/downloads/)
2) Make sure to have pip installed. You can get it by running this pythonscript (https://bootstrap.pypa.io/get-pip.py)
3) Make sure to have numpy installed: pip3 install numpy
3) Install PyInstaller using pip: pip3 install pyinstaller
4) Run PyInstaller in the directory you extracted the project's bin folder:
 Unix based:  pyinstaller kolar.py --onefile --clean -y --add-data="named_dict.pickle:." --add-binary="kolar.npy:."
 For windows: pyinstaller kolar.py --onefile --clean -y --add-data="named_dict.pickle;." --add-binary="kolar.npy;."
5) The executable is found there: ./dist/kolar
6) Make sure to have execution permissions.

For troubleshooting purpose, the script used to produce kolar.npy from Keio_collectionsheet_20.csv is provided (compile_kolar.py).
