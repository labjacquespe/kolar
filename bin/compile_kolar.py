# compile kolar.npy from Keio_collectionsheet_20.csv
import numpy as np
plaque=False
collection={}
current_plaque=[]
for i in open('./Keio_collectionsheet_20.csv','r').readlines():
    if not plaque:
        plaque = [x.strip('\n') for x in i.split(',')]
        plate_num = int(plaque[0])
        current_plaque.append(plaque)
    elif i=='\n':
        plaque=False
        collection[plate_num]=current_plaque
        current_plaque=[]
    else:
        current_plaque.append([x.strip('\n') for x in i.split(',')])
kolar=np.empty([95,9,13],dtype=object)
for r in range(95):
    try:
        arr=(np.array(collection[r],dtype=object))
        for i,j in enumerate(arr):
            kolar[r,i]=j
    except:
        pass
numpy.save('kolar.npy', kolar, allow_pickle=True)